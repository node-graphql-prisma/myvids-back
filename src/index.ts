import { GraphQLServer } from "graphql-yoga";
import { Prisma } from "prisma-binding";

// Implementation of the GraphQL schema
const resolvers = {
  Query: {
    hello: (root, args) => `Welcome ${args.name || "world"} to GraphQL training`,
    feed: (root, args, context, info) => {
      return context.dbprisma.query.videos({}, info);
    },
    video: (root, args, context, info) => {
      return context.dbprisma.query.video(
        {
          where: {
            id: args.id
          }
        },
        info
      );
    }
  },

  Mutation: {
    post: (root, args, context, info) => {
      return context.dbprisma.mutation.createVideo(
        {
          data: {
            title: args.title,
            description: args.description,
            url: args.url,
            category: args.category
          }
        },
        info
      );
    },

    delete: (root, args, context, info) => {
      return context.dbprisma.mutation.deleteVideo(
        {
          data: {
            id: args.id
          }
        },
        info
      );
    },

    update: (root, args, context, info) => {
      return context.dbprisma.mutation.updateVideo(
        {
          data: {
            title: args.title,
            description: args.description,
            url: args.url
          }
        },
        info
      );
    }
  },

  // Video: {
  //   id: (root) => root.id,
  //   title: (root) => root.title,
  //   description: (root) => root.description,
  //   url: (root) => root.url,
  //   // tag: (root) => root.title + '/' + root.description
  // }
}

// Creation of the server
const server = new GraphQLServer(
  {
    typeDefs: './src/schema.graphql',
    resolvers,
    resolverValidationOptions :{
      requireResolversForResolveType: false
    },
    context: req => ({
      ...req,
      dbprisma: new Prisma({
        typeDefs: 'src/generated/prisma.graphql',
        endpoint: process.env.PRISMA_ENDPOINT,
        secret: process.env.PRISMA_SECRET,
        debug: true
      })
    })
  }
);

// Starting the server
server.start({ port: process.env.PORT || 4000}).then(() => console.log(`Server started at port ${process.env.PORT}`));
